import httpMocks from 'node-mocks-http';
import { cacheControl } from '@helpers/middlewares/cacheControl';

describe('cacheControl', () => {
	it('should set correct Cache-Control header to response', () => {
		const correctHeader = 'no-cache, no-store, must-revalidate';
		const { req, res } = httpMocks.createMocks();

		cacheControl(req, res, () => {});

		expect(res.header('Cache-Control')).toBe(correctHeader);
	});
});
