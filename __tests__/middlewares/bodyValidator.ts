import httpMocks from 'node-mocks-http';
import { JSONSchemaType } from 'ajv';
import { AppError } from '@helpers/errors';
import { bodyValidator } from '@helpers/middlewares/bodyValidator';

describe('bodyValidator', () => {
	it('should throw error if request body is not valid', () => {
		const schema: JSONSchemaType<{ test: string }> = {
			type: 'object',
			properties: { test: { type: 'string' } },
			required: ['test'],
		};
		const { req, res } = httpMocks.createMocks({ body: { notTestField: 123 } });
		const next = jest.fn();

		bodyValidator(schema)(req, res, next);

		expect(next.mock.calls[0][0]).toBeInstanceOf(AppError);
	});

	it('should pass if request body is valid', () => {
		const schema: JSONSchemaType<{ test: string }> = {
			type: 'object',
			properties: { test: { type: 'string' } },
			required: ['test'],
		};
		const { req, res } = httpMocks.createMocks({ body: { test: 'some string' } });
		const next = jest.fn();

		bodyValidator(schema)(req, res, next);

		expect(next).toHaveBeenCalledTimes(1);
		expect(next).toHaveBeenCalledWith();
	});
});
