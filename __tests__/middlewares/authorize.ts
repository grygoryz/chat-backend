import httpMocks from 'node-mocks-http';
import { roles } from '@odm';
import { Session } from 'express-session';
import { authorize } from '@helpers/middlewares/authorize';
import { AppError } from '@helpers/errors';

describe('authorize', () => {
	it('should throw error if role is not allowed', () => {
		const { req, res } = httpMocks.createMocks();
		req.session = { user: { role: roles.user } } as Session;
		const next = jest.fn();

		authorize([roles.admin])(req, res, next);

		expect(next.mock.calls[0][0]).toBeInstanceOf(AppError);
	});

	it('should pass if role is allowed', () => {
		const { req, res } = httpMocks.createMocks({ session: { user: { role: roles.admin } } });
		const next = jest.fn();

		authorize([roles.admin])(req, res, next);

		expect(next).toHaveBeenCalledTimes(1);
		expect(next).toHaveBeenCalledWith();
	});
});
