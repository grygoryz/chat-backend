import { SocketMiddleware } from '@types';
import { acknowledgementRequired } from '@helpers/middlewares/socket/acknowledgementRequired';

describe('acknowledgementRequired', () => {
	it('should throw error if acknowledgement callback is not presented', () => {
		const event: Parameters<SocketMiddleware>[0] = ['event', 'some-data'];
		const next = jest.fn();

		acknowledgementRequired(['event'])(event, next);

		expect(next.mock.calls[0][0]).toBeInstanceOf(Error);
	});

	it('should pass if acknowledgement callback is presented', () => {
		const event: Parameters<SocketMiddleware>[0] = ['event', 'some-data', () => {}];
		const next = jest.fn();

		acknowledgementRequired(['event'])(event, next);

		expect(next).toHaveBeenCalledTimes(1);
		expect(next).toHaveBeenCalledWith();
	});
});
