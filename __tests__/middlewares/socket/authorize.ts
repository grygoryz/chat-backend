import { roles } from '@odm';
import { SocketMiddleware } from '@types';
import { authorize } from '@helpers/middlewares/socket/authorize';

describe('authorize', () => {
	it('should throw error if role is not allowed', () => {
		const event: Parameters<SocketMiddleware>[0] = ['event', 'some_data'];
		const next = jest.fn();

		authorize([{ events: ['event'], allowedRoles: [roles.admin] }], roles.user)(event, next);

		expect(next.mock.calls[0][0]).toBeInstanceOf(Error);
	});

	it('should pass if role is allowed', () => {
		const event: Parameters<SocketMiddleware>[0] = ['event', 'some_data'];
		const next = jest.fn();

		authorize([{ events: ['event'], allowedRoles: [roles.admin] }], roles.admin)(event, next);

		expect(next).toHaveBeenCalledTimes(1);
		expect(next).toHaveBeenCalledWith();
	});
});
