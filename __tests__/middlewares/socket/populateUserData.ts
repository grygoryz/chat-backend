import { UsersService } from '@services';
import { Socket } from 'socket.io';
import { IUser, roles } from '@odm';
import { populateUserData } from '@helpers/middlewares/socket/populateUserData';

jest.mock('@services/users');

const userData: Pick<IUser, 'name' | 'role' | 'about'> = { name: 'Grigory', about: { gender: 'male' }, role: roles.user };
const socket = { request: { session: { user: { id: 'some_id' } } } } as Socket;

describe('populateUserData', () => {
	it('should throw error if user not found', async () => {
		UsersService.getUserPublicData = jest.fn().mockReturnValue(Promise.reject(new Error('test')));
		const next = jest.fn();

		await populateUserData(socket, next);

		expect(next).toHaveBeenCalledWith(new Error('test'));
		expect(next).toHaveBeenCalledTimes(1);
	});

	it('should populate socket instance with user data if user found', async () => {
		UsersService.getUserPublicData = jest.fn().mockReturnValue(Promise.resolve(userData));

		await populateUserData(socket, () => {});

		expect(socket.userData).toEqual({ id: socket.request.session.user.id, ...userData });
	});

	it('should pass if user found', async () => {
		UsersService.getUserPublicData = jest.fn().mockReturnValue(Promise.resolve(userData));
		const next = jest.fn();

		await populateUserData(socket, next);

		expect(next).toHaveBeenCalledWith();
		expect(next).toHaveBeenCalledTimes(1);
	});
});
