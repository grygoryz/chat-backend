import httpMocks from 'node-mocks-http';
import { authenticate } from '@helpers/middlewares/authenticate';
import { AppError } from '@helpers/errors';

describe('authenticate', () => {
	it('should throw error if session cookie does not exist', () => {
		const { req, res } = httpMocks.createMocks({ session: {} });
		const next = jest.fn();

		authenticate(req, res, next);

		expect(next.mock.calls[0][0]).toBeInstanceOf(AppError);
	});

	it('should pass if session cookie includes id of user', () => {
		const { req, res } = httpMocks.createMocks({ session: { user: { id: 'someId' } } });
		const next = jest.fn();

		authenticate(req, res, next);

		expect(next).toHaveBeenCalledTimes(1);
		expect(next).toHaveBeenCalledWith();
	});
});
