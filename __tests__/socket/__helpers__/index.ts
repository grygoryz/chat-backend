import { IChatUser } from '@socket/types';
import { roles } from '@odm';

export const createSocketMock = <T extends Record<string, any>>(options: T) => {
	return {
		id: 'ojIckSD2jqNzOqIrAGzL',
		broadcast: {
			emit: jest.fn(),
		},
		to: jest.fn().mockReturnThis(),
		emit: jest.fn(),
		join: jest.fn(),
		disconnect: jest.fn(),
		...options,
	};
};

export const createServerMock = () => {
	return {
		to: jest.fn().mockReturnThis(),
		in: jest.fn().mockReturnThis(),
		allSockets: jest.fn(),
		emit: jest.fn(),
	};
};

export const getUserData: () => IChatUser = () => ({
	id: '609ac2f481aac66b6543d170',
	name: 'Grigory',
	about: { gender: 'male' },
	role: roles.user,
});
