import { Socket } from 'socket.io';
import { validate } from '@helpers';
import { ChatStorage } from '@socket/chatStorage';
import { getMessagesList, sendMessage } from '@socket/handlers/messages/handlers';
import { IMessage } from '@socket/types';
import { customEvents } from '@socket/mappings';
import { generateRandomHexString } from '@helpers/utils';
import { createSocketMock, getUserData } from '../__helpers__';

jest.mock('@socket/chatStorage');
jest.mock('@db/redis');
jest.mock('ioredis');
jest.mock('@helpers/validate', () => ({
	validate: jest.fn(),
}));
jest.mock('@helpers/utils/generateRandomHexString', () => ({
	generateRandomHexString: jest.fn(),
}));

const userData = getUserData();

afterEach(() => {
	jest.clearAllMocks();
});

describe('messages handlers', () => {
	describe('messagesList', () => {
		it('should send messages list on success', async () => {
			const messagesData: { messages: Array<IMessage>; total: number } = {
				messages: [{ id: 'message_id', name: userData.name, userId: userData.id, date: Date.now(), text: 'Hello' }],
				total: 1,
			};
			ChatStorage.getMessages = jest.fn().mockReturnValue(Promise.resolve(messagesData));
			const cb = jest.fn();

			await getMessagesList(0, cb);

			expect(cb).toHaveBeenCalledWith(null, messagesData);
		});

		it('should handle error', async () => {
			const errorMessage = 'test';
			ChatStorage.getMessages = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
			const cb = jest.fn();

			await getMessagesList(0, cb);

			expect(cb).toHaveBeenCalledWith(errorMessage);
		});
	});

	describe('message', () => {
		it('should prevent sending if user is muted', async () => {
			const timeout = 60;
			const errorMessage = `You are muted for ${timeout / 60} minutes`;
			ChatStorage.getUserMuteTimeout = jest.fn().mockReturnValueOnce(Promise.resolve(timeout));
			const socket = createSocketMock({ userData: getUserData() });
			const cb = jest.fn();

			await sendMessage((socket as unknown) as Socket)({ text: 'Hello' }, cb);

			expect(cb).toHaveBeenCalledWith(errorMessage);
			expect(ChatStorage.addMessage).not.toHaveBeenCalled();
			expect(socket.broadcast.emit.mock.calls.some(c => c[0] === customEvents.message)).toBe(false);
		});

		it('should prevent sending if message is not valid', async () => {
			const cb = jest.fn();
			const validationErrors = ['error'];
			const socket = createSocketMock({ userData: getUserData() });
			(validate as jest.Mock).mockReturnValueOnce(validationErrors);

			await sendMessage((socket as unknown) as Socket)({ text: 'Hello' }, cb);

			expect(cb).toHaveBeenCalledWith(validationErrors);
			expect(ChatStorage.addMessage).not.toHaveBeenCalled();
			expect(socket.broadcast.emit.mock.calls.some(c => c[0] === customEvents.message)).toBe(false);
		});

		it('should handle success', async () => {
			const messageData = { id: 'message_id', text: 'Hello', name: userData.name, date: Date.now(), userId: userData.id };
			ChatStorage.addMessage = jest.fn().mockReturnValue(Promise.resolve());
			(generateRandomHexString as jest.Mock).mockReturnValue(Promise.resolve(messageData.id));
			global.Date.now = jest.fn().mockReturnValue(messageData.date);
			const socket = createSocketMock({ userData: getUserData() });
			const cb = jest.fn();

			await sendMessage((socket as unknown) as Socket)({ text: 'Hello' }, cb);

			expect(ChatStorage.addMessage).toHaveBeenCalledWith(messageData);
			expect(socket.broadcast.emit).toHaveBeenCalledWith(customEvents.message, messageData);
			expect(cb).toHaveBeenCalledWith(null, messageData);
		});

		it('should handle error', async () => {
			const errorMessage = 'test';
			ChatStorage.addMessage = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
			(generateRandomHexString as jest.Mock).mockReturnValue(Promise.resolve());
			const socket = createSocketMock({ userData: getUserData() });
			const cb = jest.fn();

			await sendMessage((socket as unknown) as Socket)({ text: 'Hello' }, cb);

			expect(cb).toHaveBeenCalledWith(errorMessage);
		});
	});
});
