import { Socket } from 'socket.io';
import { UsersService } from '@services';
import { ChatStorage } from '@socket/chatStorage';
import { customEvents } from '@socket/mappings';
import { addTypingUser, muteUser, removeTypingUser, updateUserBio } from '@socket/handlers/users/handlers';
import { createSocketMock, getUserData } from '../__helpers__';

jest.mock('@socket/chatStorage');
jest.mock('@db/redis');
jest.mock('ioredis');
jest.mock('@services/users');

const userData = getUserData();

afterEach(() => {
	jest.clearAllMocks();
});

describe('users handlers', () => {
	describe('userTyping', () => {
		it('should add typing user to storage and notify others by broadcast on success', async () => {
			const cb = jest.fn();
			const socket = createSocketMock({ userData: getUserData() });

			await addTypingUser((socket as unknown) as Socket)(cb);

			expect(ChatStorage.addTypingUser).toHaveBeenCalledWith(userData.id);
			expect(socket.broadcast.emit).toHaveBeenCalledWith(customEvents.userTyping, userData.id);
		});

		it('should handle error', async () => {
			const errorMessage = 'test';
			ChatStorage.addTypingUser = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
			const socket = createSocketMock({ userData: getUserData() });
			const cb = jest.fn();

			await addTypingUser((socket as unknown) as Socket)(cb);

			expect(cb).toHaveBeenCalledWith(errorMessage);
		});
	});

	describe('userTypingEnd', () => {
		it('should remove typing user from storage and notify others by broadcast on success', async () => {
			const cb = jest.fn();
			const socket = createSocketMock({ userData: getUserData() });

			await removeTypingUser((socket as unknown) as Socket)(cb);

			expect(ChatStorage.removeTypingUser).toHaveBeenCalledWith(userData.id);
			expect(socket.broadcast.emit).toHaveBeenCalledWith(customEvents.userTypingEnd, userData.id);
		});

		it('should handle error', async () => {
			const errorMessage = 'test';
			ChatStorage.removeTypingUser = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
			const cb = jest.fn();
			const socket = createSocketMock({ userData: getUserData() });

			await removeTypingUser((socket as unknown) as Socket)(cb);

			expect(cb).toHaveBeenCalledWith(errorMessage);
		});
	});

	describe('userUpdateBio', () => {
		it('should update bio in users service and chat storage on success', async () => {
			const cb = jest.fn();
			const bio = 'test_bio';
			const socket = createSocketMock({ userData: getUserData() });

			await updateUserBio((socket as unknown) as Socket)(bio, cb);

			expect(UsersService.updateBio).toHaveBeenCalledWith(userData.id, bio);
			expect(ChatStorage.updateUserData).toHaveBeenCalledWith({ ...userData, about: { ...userData.about, bio } });
		});

		it('should populate socket userData with new bio on success', async () => {
			const cb = jest.fn();
			const bio = 'test_bio';
			const socket = createSocketMock({ userData: getUserData() });

			await updateUserBio((socket as unknown) as Socket)(bio, cb);

			expect(socket.userData).toEqual({ ...userData, about: { ...userData.about, bio } });
		});

		it('should send new bio to user and notify other users by broadcast on success', async () => {
			const cb = jest.fn();
			const bio = 'test_bio';
			const socket = createSocketMock({ userData: getUserData() });

			await updateUserBio((socket as unknown) as Socket)(bio, cb);

			expect(cb).toHaveBeenCalledWith(null, bio);
			expect(socket.broadcast.emit).toHaveBeenCalledWith(customEvents.userUpdateBio, { id: userData.id, bio });
		});

		it('should handle error', async () => {
			const errorMessage = 'test';
			ChatStorage.updateUserData = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
			const socket = createSocketMock({ userData: getUserData() });
			const cb = jest.fn();

			await updateUserBio((socket as unknown) as Socket)('bio', cb);

			expect(cb).toHaveBeenCalledWith(errorMessage);
		});
	});

	describe('userMute', () => {
		it('should mute specific user on specified timeout and send confirmation on success', async () => {
			const cb = jest.fn();
			const id = 'user_to_mute';
			const timeout = 2000;

			await muteUser(id, timeout, cb);

			expect(ChatStorage.muteUser).toHaveBeenCalledWith(id, timeout);
			expect(cb).toHaveBeenCalledWith(null, true);
		});

		it('should handle error', async () => {
			const errorMessage = 'test';
			ChatStorage.muteUser = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
			const cb = jest.fn();

			await muteUser('user_to_mute', 2000, cb);

			expect(cb).toHaveBeenCalledWith(errorMessage);
		});
	});
});
