import { Server, Socket } from 'socket.io';
import { ChatStorage } from '@socket/chatStorage';
import { handleConnection, handleDisconnect, handleError } from '@socket/handlers/main/handlers';
import { customEvents } from '@socket/mappings';
import { createServerMock, createSocketMock, getUserData } from '../__helpers__';

jest.mock('@socket/chatStorage');
jest.mock('@db/redis');
jest.mock('ioredis');

const userData = getUserData();

afterEach(() => {
	jest.clearAllMocks();
});

describe('main handlers', () => {
	describe('connect', () => {
		it('should add user to storage and notify other users if user was not connected', async () => {
			ChatStorage.isUserConnected = jest.fn().mockReturnValue(Promise.resolve(false));
			const socket = createSocketMock({ userData: getUserData() });
			const io = createServerMock();

			await handleConnection((io as unknown) as Server, (socket as unknown) as Socket);

			expect(ChatStorage.addUser).toHaveBeenCalledTimes(1);
			expect(socket.broadcast.emit).toHaveBeenCalledWith(customEvents.userConnected, userData);
		});

		it('should pass adding to storage and notifying other users if user was connected', async () => {
			ChatStorage.isUserConnected = jest.fn().mockReturnValue(Promise.resolve(true));
			const socket = createSocketMock({ userData: getUserData() });
			const io = createServerMock();

			await handleConnection((io as unknown) as Server, (socket as unknown) as Socket);

			expect(ChatStorage.addUser).not.toHaveBeenCalled();
			expect(socket.broadcast.emit).not.toHaveBeenCalledWith(customEvents.userConnected, userData);
		});

		it('should join socket to room associated with user id', async () => {
			const socket = createSocketMock({ userData: getUserData() });
			const io = createServerMock();

			await handleConnection((io as unknown) as Server, (socket as unknown) as Socket);

			expect(socket.join).toHaveBeenCalledWith(userData.id);
		});

		it('should fetch initial data from storage and send to client', async () => {
			ChatStorage.getUsers = jest.fn().mockReturnValue(Promise.resolve([userData]));
			ChatStorage.getMessages = jest.fn().mockReturnValue(Promise.resolve([]));
			ChatStorage.getTypingUsers = jest.fn().mockReturnValue(Promise.resolve([]));
			const socket = createSocketMock({ userData: getUserData() });
			const io = createServerMock();
			const socketId = socket.id;

			await handleConnection((io as unknown) as Server, (socket as unknown) as Socket);

			expect(io.to).toHaveBeenCalledWith(socketId);
			expect(io.emit).toHaveBeenCalledWith(customEvents.initialData, { users: [userData], messages: [], typingUsers: [] });
		});

		it('should handle error', async () => {
			const errorMessage = 'test';
			ChatStorage.isUserConnected = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
			const socket = createSocketMock({ userData: getUserData() });
			const io = createServerMock();

			await handleConnection((io as unknown) as Server, (socket as unknown) as Socket);

			expect(socket.emit).toHaveBeenCalledWith(customEvents.error, errorMessage);
			expect(socket.disconnect).toHaveBeenCalledWith(true);
		});
	});

	describe('disconnect', () => {
		it("should properly cleanup if no sockets left in room associated with user's id", async () => {
			const io = createServerMock();
			io.allSockets.mockReturnValue(Promise.resolve({ size: 0 }));
			const socket = createSocketMock({ userData: getUserData() });

			await handleDisconnect((io as unknown) as Server, (socket as unknown) as Socket)();

			expect(io.emit).toHaveBeenCalledWith(customEvents.userDisconnected, userData.id);
			expect(ChatStorage.removeUser).toHaveBeenCalledWith(userData.id);
			expect(ChatStorage.removeUser).toHaveBeenCalledWith(userData.id);
		});

		it('should ignore cleanup if room associated with user id is not empty', async () => {
			const io = createServerMock();
			io.allSockets.mockReturnValue(Promise.resolve({ size: 1 }));
			const socket = createSocketMock({ userData: getUserData() });

			await handleDisconnect((io as unknown) as Server, (socket as unknown) as Socket)();

			expect(io.emit).not.toHaveBeenCalledWith(customEvents.userDisconnected, userData.id);
			expect(ChatStorage.removeUser).not.toHaveBeenCalledWith(userData.id);
			expect(ChatStorage.removeUser).not.toHaveBeenCalledWith(userData.id);
		});
	});

	describe('error', () => {
		it('should emit user with error message and disconnect socket', async () => {
			const errorMessage = 'test';
			const socket = createSocketMock({ userData: getUserData() });

			await handleError((socket as unknown) as Socket)(new Error(errorMessage));

			expect(socket.emit).toHaveBeenCalledWith(customEvents.error, errorMessage);
			expect(socket.disconnect).toHaveBeenCalledWith(true);
		});
	});
});
