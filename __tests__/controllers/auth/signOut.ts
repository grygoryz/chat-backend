import httpMocks from 'node-mocks-http';
import { Error } from 'mongoose';
import { Session } from 'express-session';
import { signOut } from '@routes/auth/controllers';

jest.mock('@services/auth');

const reqOptions = { session: {} } as { session: Session };

describe('signOut', () => {
	it('should send 204 status on success', async () => {
		const { req, res } = httpMocks.createMocks(reqOptions);
		res.sendStatus = jest.fn(res.sendStatus);
		req.session.destroy = jest.fn(callback => {
			callback(null);
			return req.session;
		});

		await signOut(req, res);

		expect(res.sendStatus).toHaveBeenCalledWith(204);
	});

	it('should handle error', async () => {
		const { req, res } = httpMocks.createMocks(reqOptions);
		res.status = jest.fn(res.sendStatus);
		res.json = jest.fn(res.json);
		const errorMessage = 'test';
		req.session.destroy = jest.fn(callback => {
			callback(new Error('test'));
			return req.session;
		});

		await signOut(req, res);

		expect(res.status).toHaveBeenCalledWith(400);
		expect(res.json).toHaveBeenCalledWith({ message: errorMessage });
	});
});
