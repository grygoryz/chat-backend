import httpMocks, { RequestOptions } from 'node-mocks-http';
import { AuthService } from '@services';
import { Error } from 'mongoose';
import { roles } from '@odm';
import { Session } from 'express-session';
import { verifyUser } from '@routes/auth/controllers';

jest.mock('@services/auth');

let reqOptions: RequestOptions;

beforeEach(() => {
	reqOptions = { session: { user: { role: roles.unverified } } } as { session: Session };
});

describe('verifyUser', () => {
	it('should send status 200 on success', async () => {
		AuthService.verifyUser = jest.fn().mockReturnValue(Promise.resolve());
		const { req, res } = httpMocks.createMocks(reqOptions);
		res.sendStatus = jest.fn(res.sendStatus);

		await verifyUser(req, res);

		expect(res.sendStatus).toHaveBeenCalledWith(200);
	});

	it('should set correct role to session cookie on success', async () => {
		AuthService.verifyUser = jest.fn().mockReturnValue(Promise.resolve());
		const { req, res } = httpMocks.createMocks(reqOptions);
		await verifyUser(req, res);

		expect(req.session.user.role).toBe(roles.user);
	});

	it('should handle error', async () => {
		const errorMessage = 'test';
		AuthService.verifyUser = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
		const { req, res } = httpMocks.createMocks(reqOptions);
		res.status = jest.fn(res.status);
		res.json = jest.fn(res.json);

		await verifyUser(req, res);

		expect(req.session.user.role).toBe(roles.unverified);
		expect(res.status).toHaveBeenCalledWith(400);
		expect(res.json).toHaveBeenCalledWith({ message: errorMessage });
	});
});
