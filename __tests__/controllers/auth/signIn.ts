import httpMocks, { RequestOptions } from 'node-mocks-http';
import { AuthService } from '@services';
import { Error } from 'mongoose';
import { roles } from '@odm';
import { signIn } from '@routes/auth/controllers';

jest.mock('@services/auth');

const userData = { id: 'some_id', role: roles.user, name: 'Grigory', about: { gender: 'male' } };
let reqOptions: RequestOptions;

beforeEach(() => {
	reqOptions = { session: {} };
});

describe('signIn', () => {
	it('should send user data on success', async () => {
		AuthService.signIn = jest.fn().mockReturnValue(Promise.resolve(userData));
		const { req, res } = httpMocks.createMocks(reqOptions);
		res.status = jest.fn(res.status);
		res.json = jest.fn(res.json);

		await signIn(req, res);

		expect(res.status).toHaveBeenCalledWith(200);
		expect(res.json).toHaveBeenCalledWith(userData);
	});

	it('should populate session cookie on success', async () => {
		AuthService.signIn = jest.fn().mockReturnValue(Promise.resolve(userData));
		const { req, res } = httpMocks.createMocks(reqOptions);

		await signIn(req, res);

		expect(req.session.user).toEqual({ id: userData.id, role: userData.role });
	});

	it('should handle error', async () => {
		const errorMessage = 'test';
		AuthService.signIn = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
		const { req, res } = httpMocks.createMocks(reqOptions);
		res.status = jest.fn(res.status);
		res.json = jest.fn(res.json);

		await signIn(req, res);

		expect(req.session.user).toBeUndefined();
		expect(res.status).toHaveBeenCalledWith(400);
		expect(res.json).toHaveBeenCalledWith({ message: errorMessage });
	});
});
