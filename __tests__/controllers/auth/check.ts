import httpMocks from 'node-mocks-http';
import { AuthService } from '@services';
import { Error } from 'mongoose';
import { roles } from '@odm';
import { Session } from 'express-session';
import { check } from '@routes/auth/controllers';

jest.mock('@services/auth');

const checkResponse = { role: roles.user, name: 'Grigory', about: { gender: 'male' } };
const reqOptions = { session: { user: { id: 'some_id' } } } as { session: Session };

describe('check', () => {
	it('should send user data on success', async () => {
		AuthService.check = jest.fn().mockReturnValue(Promise.resolve(checkResponse));
		const { req, res } = httpMocks.createMocks(reqOptions);
		res.status = jest.fn(res.status);
		res.json = jest.fn(res.json);

		await check(req, res);

		expect(res.status).toHaveBeenCalledWith(200);
		expect(res.json).toHaveBeenCalledWith({ ...checkResponse, id: req.session.user.id });
	});

	it('should handle error', async () => {
		const errorMessage = 'test';
		AuthService.check = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
		const { req, res } = httpMocks.createMocks(reqOptions);
		res.status = jest.fn(res.sendStatus);
		res.json = jest.fn(res.json);

		await check(req, res);

		expect(res.status).toHaveBeenCalledWith(400);
		expect(res.json).toHaveBeenCalledWith({ message: errorMessage });
	});
});
