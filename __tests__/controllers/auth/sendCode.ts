import httpMocks from 'node-mocks-http';
import { AuthService } from '@services';
import { Error } from 'mongoose';
import { Session } from 'express-session';
import { sendCode } from '@routes/auth/controllers';

jest.mock('@services/auth');

const reqOptions = { session: { user: {} } } as { session: Session };

describe('sendCode', () => {
	it('should send status 200 on success', async () => {
		AuthService.sendCode = jest.fn().mockReturnValue(Promise.resolve());
		const { req, res } = httpMocks.createMocks(reqOptions);
		res.sendStatus = jest.fn(res.sendStatus);

		await sendCode(req, res);

		expect(res.sendStatus).toHaveBeenCalledWith(200);
	});

	it('should handle error', async () => {
		const errorMessage = 'test';
		AuthService.sendCode = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
		const { req, res } = httpMocks.createMocks(reqOptions);
		res.status = jest.fn(res.status);
		res.json = jest.fn(res.json);

		await sendCode(req, res);

		expect(res.status).toHaveBeenCalledWith(400);
		expect(res.json).toHaveBeenCalledWith({ message: errorMessage });
	});
});
