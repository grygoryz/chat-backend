import httpMocks from 'node-mocks-http';
import { AuthService } from '@services';
import { Error } from 'mongoose';
import { register } from '@routes/auth/controllers';

jest.mock('@services/auth');

describe('register', () => {
	it('should send status 200 on success', async () => {
		AuthService.register = jest.fn().mockReturnValue(Promise.resolve());
		const { req, res } = httpMocks.createMocks();
		res.sendStatus = jest.fn(res.sendStatus);

		await register(req, res);

		expect(res.sendStatus).toHaveBeenCalledWith(200);
	});

	it('should handle error', async () => {
		const errorMessage = 'test';
		AuthService.register = jest.fn().mockReturnValue(Promise.reject(new Error(errorMessage)));
		const { req, res } = httpMocks.createMocks();
		res.status = jest.fn(res.status);
		res.json = jest.fn(res.json);

		await register(req, res);

		expect(res.status).toHaveBeenCalledWith(400);
		expect(res.json).toHaveBeenCalledWith({ message: errorMessage });
	});
});
