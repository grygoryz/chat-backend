import httpMocks from 'node-mocks-http';
import { uploadFile } from '@routes/storage/controllers';

jest.mock('@services/auth');

describe('uploadFile', () => {
	it('should send 201 status and correct file data if file exists in req', () => {
		const file: Pick<Express.Multer.File, 'filename' | 'path'> = { filename: 'test.png', path: 'public/storage/test.png' };
		const { req, res } = httpMocks.createMocks({ file });
		res.status = jest.fn(res.status);
		res.json = jest.fn(res.json);

		uploadFile(req, res);

		expect(res.status).toHaveBeenCalledWith(201);
		expect(res.json).toHaveBeenCalledWith({ name: 'test.png', src: '/storage/test.png' });
	});

	it('should handle error if file does not exist in req', async () => {
		const errorMessage = 'Error while file uploading';
		const { req, res } = httpMocks.createMocks({ file: null });
		res.status = jest.fn(res.sendStatus);
		res.json = jest.fn(res.json);

		await uploadFile(req, res);

		expect(res.status).toHaveBeenCalledWith(400);
		expect(res.json).toHaveBeenCalledWith({ message: errorMessage });
	});
});
