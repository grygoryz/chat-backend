import { ServerOptions } from 'socket.io';

const { SIO_PATH, FRONT_HOST } = process.env;

export const socketIOConfig: Partial<ServerOptions> = {
	connectTimeout: 15000,
	path: SIO_PATH,
	cors: {
		origin: FRONT_HOST,
		methods: ['GET', 'POST'],
		credentials: true,
	},
	serveClient: false,
	transports: ['websocket'],
};
