import session, { SessionOptions } from 'express-session';
import connectRedis from 'connect-redis';
import { client as redisClient } from '@db/redis';

const RedisStore = connectRedis(session);

const SessionsStore = new RedisStore({ client: redisClient });

const { SESSION_SECRET = 'FALLBACK_SECRET', NODE_ENV } = process.env;

export const sessionOptions: SessionOptions = {
	secret: SESSION_SECRET,
	store: SessionsStore,
	name: 'sessionId',
	rolling: true,
	saveUninitialized: true,
	resave: false,
	cookie: {
		httpOnly: true,
		maxAge: 20 * 60 * 1000, // 20 minutes,
		secure: NODE_ENV === 'production',
	},
};
