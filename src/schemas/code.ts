import { JSONSchemaType } from 'ajv';
import { ICode } from '@odm';

export const code: JSONSchemaType<ICode, true> = {
	type: 'object',
	properties: {
		value: {
			type: 'string',
		},
	},
	required: ['value'],
	additionalProperties: false,
};
