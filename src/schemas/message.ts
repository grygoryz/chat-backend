import { JSONSchemaType } from 'ajv';
import { IMessage } from '@socket/types';
import { messageLength } from '@socket/chatStorage';

const { max } = messageLength;

export const message: JSONSchemaType<IMessage, true> = {
	type: 'object',
	properties: {
		text: {
			type: 'string',
			maxLength: max,
		},
		file: {
			type: 'object',
			nullable: true,
			properties: {
				name: {
					type: 'string',
				},
				src: {
					type: 'string',
				},
			},
			required: ['name', 'src'],
		},
	},
	required: ['text'],
	additionalProperties: false,
};
