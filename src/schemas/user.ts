import { JSONSchemaType } from 'ajv';
import { usersMappings, IUser } from '@odm';
import { IUserCredentials } from '@types';

const { nameLength, descriptionLength, genders } = usersMappings;

export const user: JSONSchemaType<IUser> = {
	type: 'object',
	properties: {
		name: {
			type: 'string',
			minLength: nameLength.min,
			maxLength: nameLength.max,
		},
		email: {
			type: 'string',
		},
		password: {
			type: 'string',
		},
		about: {
			type: 'object',
			properties: {
				gender: {
					type: 'string',
					// @ts-ignore
					enum: genders,
				},
				bio: {
					type: 'string',
					minLength: descriptionLength.min,
					maxLength: descriptionLength.max,
					nullable: true,
				},
			},
			required: ['gender'],
			additionalProperties: false,
		},
	},
	required: ['email', 'name', 'password', 'about'],
	additionalProperties: false,
};

export const credentials: JSONSchemaType<IUserCredentials> = {
	type: 'object',
	properties: {
		email: {
			type: 'string',
		},
		password: {
			type: 'string',
		},
	},
	required: ['email', 'password'],
	additionalProperties: false,
};
