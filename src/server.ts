import express, { Request, Response, NextFunction } from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import { session, cacheControl, rateLimiter } from '@helpers/middlewares';
import { auth, storage } from '@routes';
import { AppError } from '@helpers/errors';
import contentDisposition from 'content-disposition';
import morgan from 'morgan';

import * as path from 'path';

const { FRONT_HOST = 'http://localhost:3005', NODE_ENV } = process.env;

const app = express();

if (NODE_ENV === 'production') {
	app.set('trust proxy', 1);
}

// middlewares
app.use(morgan('tiny'));
app.use(
	cors({
		origin: FRONT_HOST,
		credentials: true,
	})
);
app.use(rateLimiter);
app.use(cacheControl);
app.use(session);
app.use(cookieParser());
app.use(bodyParser.json({ limit: '10kb' }));
app.use(
	'/storage',
	express.static(path.resolve('public/storage'), {
		setHeaders: (res, filePath) => {
			res.setHeader('Content-Disposition', contentDisposition(filePath));
		},
	})
);

// routes
app.use('/auth', auth);
app.use('/storage', storage);

app.use('*', (req, res, next) => {
	const error = new Error(`Can not find right route for method ${req.method} and path ${req.originalUrl}`);
	next(error);
});

// error handling middleware
app.use((err: AppError, req: Request, res: Response, next: NextFunction) => {
	res.status(err.statusCode || 500).json({ message: err.message });
});

export { app };
