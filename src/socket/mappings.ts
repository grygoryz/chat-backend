export const customEvents = {
	userConnected: 'users:user_connected',
	userDisconnected: 'users:user_disconnected',
	userTyping: 'users:user_typing',
	userTypingEnd: 'users:user_typing_end',
	userUpdateBio: 'users:user_update_bio',
	userMute: 'users:mute',
	messagesList: 'messages:list',
	message: 'messages:message',
	initialData: 'common:initial_data',
	error: 'common:error',
};

export const messagesPageSize = 12;
