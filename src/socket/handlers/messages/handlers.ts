import { AcknowledgementCb, IMessage } from '@socket/types';
import { ChatStorage } from '@socket/chatStorage';
import { customEvents, messagesPageSize } from '@socket/mappings';
import { validate } from '@helpers';
import { message as messageSchema } from '@schemas';
import { generateRandomHexString } from '@helpers/utils';
import { Socket } from 'socket.io';

export const getMessagesList = async (start: number, cb: AcknowledgementCb<{ messages: Array<IMessage>; total: number }>) => {
	try {
		const messagesData = await ChatStorage.getMessages(start, messagesPageSize);
		cb(null, messagesData);
	} catch ({ message }) {
		cb(message);
	}
};

export const sendMessage = (socket: Socket) => async (data: Pick<IMessage, 'text' | 'file'>, cb: AcknowledgementCb<IMessage>) => {
	try {
		const { id: userId, name } = socket.userData;
		const muteTimeout = await ChatStorage.getUserMuteTimeout(userId);
		if (muteTimeout) {
			const minutesLeft = Math.ceil(muteTimeout / 60);
			return cb(`You are muted for ${minutesLeft} minutes`);
		}

		const errors = validate(messageSchema, data);
		if (errors) return cb(errors);

		const id = await generateRandomHexString(8);
		const { text, file } = data;
		const messageData = {
			id,
			userId,
			text,
			name,
			...(file && { file }),
			date: Date.now(),
		};
		await ChatStorage.addMessage(messageData);
		socket.broadcast.emit(customEvents.message, messageData);
		cb(null, messageData);
	} catch ({ message }) {
		cb(message);
	}
};
