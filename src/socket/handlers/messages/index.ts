import { Server, Socket } from 'socket.io';
import { roles } from '@odm';
import { applySocketMiddlewares } from '@helpers';
import { customEvents } from '@socket/mappings';
import { acknowledgementRequired, authorize } from '@helpers/middlewares/socket';
import { getMessagesList, sendMessage } from '@socket/handlers/messages/handlers';

export const registerMessagesHandlers = (io: Server, socket: Socket) => {
	applySocketMiddlewares(socket, [
		acknowledgementRequired([customEvents.messagesList, customEvents.message]),
		authorize([{ events: [customEvents.message], allowedRoles: [roles.user, roles.admin] }], socket.userData.role),
	]);

	socket.on(customEvents.messagesList, getMessagesList);
	socket.on(customEvents.message, sendMessage(socket));
};
