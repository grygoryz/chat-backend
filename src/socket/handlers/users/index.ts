import { roles } from '@odm';
import { Server, Socket } from 'socket.io';
import { customEvents } from '@socket/mappings';
import { applySocketMiddlewares } from '@helpers';
import { acknowledgementRequired, authorize } from '@helpers/middlewares/socket';
import { addTypingUser, muteUser, removeTypingUser, updateUserBio } from '@socket/handlers/users/handlers';

export const registerUsersHandlers = (io: Server, socket: Socket) => {
	applySocketMiddlewares(socket, [
		acknowledgementRequired([
			customEvents.userTyping,
			customEvents.userTypingEnd,
			customEvents.userUpdateBio,
			customEvents.userMute,
		]),
		authorize(
			[
				{ events: [customEvents.userTyping, customEvents.userTypingEnd], allowedRoles: [roles.user, roles.admin] },
				{ events: [customEvents.userMute], allowedRoles: [roles.admin] },
			],
			socket.userData.role
		),
	]);

	socket.on(customEvents.userTyping, addTypingUser(socket));
	socket.on(customEvents.userTypingEnd, removeTypingUser(socket));
	socket.on(customEvents.userUpdateBio, updateUserBio(socket));
	socket.on(customEvents.userMute, muteUser);
};
