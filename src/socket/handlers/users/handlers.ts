import { AcknowledgementCb } from '@socket/types';
import { ChatStorage } from '@socket/chatStorage';
import { customEvents } from '@socket/mappings';
import { Socket } from 'socket.io';
import { UsersService } from '@services';

export const addTypingUser = (socket: Socket) => async (cb: AcknowledgementCb<true>) => {
	const { id } = socket.userData;
	try {
		await ChatStorage.addTypingUser(id);
		socket.broadcast.emit(customEvents.userTyping, id);
		cb(null, true);
	} catch ({ message }) {
		cb(message);
	}
};

export const removeTypingUser = (socket: Socket) => async (cb: AcknowledgementCb<true>) => {
	const { id } = socket.userData;
	try {
		await ChatStorage.removeTypingUser(id);
		socket.broadcast.emit(customEvents.userTypingEnd, id);
		cb(null, true);
	} catch ({ message }) {
		cb(message);
	}
};

export const updateUserBio = (socket: Socket) => async (bio: string, cb: AcknowledgementCb<string>) => {
	try {
		const { id } = socket.userData;
		await UsersService.updateBio(id, bio);
		socket.userData.about.bio = bio;
		await ChatStorage.updateUserData(socket.userData);
		socket.broadcast.emit(customEvents.userUpdateBio, { id, bio });
		cb(null, bio);
	} catch ({ message }) {
		cb(message);
	}
};

export const muteUser = async (id: string, timeout: number, cb: AcknowledgementCb<true>) => {
	try {
		await ChatStorage.muteUser(id, timeout);
		cb(null, true);
	} catch ({ message }) {
		cb(message);
	}
};
