import { Server, Socket } from 'socket.io';
import { ChatStorage } from '@socket/chatStorage';
import { customEvents, messagesPageSize } from '@socket/mappings';
import debug from 'debug';

const dg = debug('socket.io');

export const handleConnection = async (io: Server, socket: Socket) => {
	try {
		const isConnected = await ChatStorage.isUserConnected(socket.userData.id);
		if (!isConnected) {
			dg(`User connected with id - ${socket.userData.id}`);
			await ChatStorage.addUser(socket.userData);
			socket.broadcast.emit(customEvents.userConnected, socket.userData);
		}
		socket.join(socket.userData.id);

		const [users, messages, typingUsers] = await Promise.all([
			ChatStorage.getUsers(),
			ChatStorage.getMessages(0, messagesPageSize),
			ChatStorage.getTypingUsers(),
		]);
		io.to(socket.id).emit(customEvents.initialData, { users, messages, typingUsers });
	} catch ({ message }) {
		socket.emit(customEvents.error, message);
		socket.disconnect(true);
	}
};

export const handleDisconnect = (io: Server, socket: Socket) => async () => {
	const { id } = socket.userData;
	const matchingSockets = await io.in(id).allSockets();
	const isDisconnected = matchingSockets.size === 0;
	if (isDisconnected) {
		io.emit(customEvents.userDisconnected, id);
		await Promise.all([ChatStorage.removeUser(id), ChatStorage.removeTypingUser(id)]);
		dg(`User disconnected with id - ${socket.userData.id}`);
	}
};

export const handleError = (socket: Socket) => ({ message }: Error) => {
	socket.emit(customEvents.error, message);
	socket.disconnect(true);
};
