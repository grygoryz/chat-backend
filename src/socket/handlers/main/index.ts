import { Server, Socket } from 'socket.io';
import { handleConnection, handleDisconnect, handleError } from './handlers';

export const registerMainHandlers = async (io: Server, socket: Socket) => {
	await handleConnection(io, socket);

	socket.on('disconnect', handleDisconnect(io, socket));
	socket.on('error', handleError(socket));
};
