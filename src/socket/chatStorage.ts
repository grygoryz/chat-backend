import { client as redisClient } from '@db/redis';
import { IChatUser, IMessage } from './types';
import { mockMessages } from './mock';

const maxMessagesCount = 40;

export const messageLength = {
	max: 255,
};

export class ChatStorage {
	public static getUsers(): Promise<Array<IChatUser>> {
		return new Promise((resolve, reject) => {
			const ids = new Set<string>();
			const commands: Array<Array<string>> = [];

			const stream = redisClient.sscanStream('chat:users');
			stream.on('data', (resultIds: Array<string>) => {
				resultIds.forEach(id => {
					if (!ids.has(id)) {
						ids.add(id);
						commands.push(['get', `chat:users:${id}`]);
					}
				});
			});
			stream.on('end', async () => {
				const results = await redisClient.multi(commands).exec();
				const users = results.map(([err, user]) => (err ? null : JSON.parse(user))).filter(v => !!v);
				resolve(users);
			});
			stream.on('error', err => {
				reject(err);
			});
		});
	}

	public static addUser(data: IChatUser) {
		return redisClient.multi().sadd('chat:users', data.id).set(`chat:users:${data.id}`, JSON.stringify(data)).exec();
	}

	public static async getUser(id: string) {
		const user = await redisClient.get(`chat:users:${id}`);

		return user ? (JSON.parse(user) as IChatUser) : null;
	}

	public static updateUserData(data: IChatUser) {
		return redisClient.set(`chat:users:${data.id}`, JSON.stringify(data));
	}

	public static removeUser(id: string) {
		return redisClient.multi().del(`chat:users:${id}`).srem('chat:users', id).exec();
	}

	public static async isUserConnected(id: string) {
		const result = await redisClient.exists(`chat:users:${id}`);

		return !!result;
	}

	public static addMessage(data: IMessage) {
		return redisClient
			.multi()
			.lpush('chat:messages', JSON.stringify(data))
			.ltrim('chat:messages', 0, maxMessagesCount - 1)
			.exec();
	}

	public static async getMessages(start: number, count: number) {
		const [[messagesErr, messages], [lengthErr, length]] = await redisClient
			.multi()
			.lrange('chat:messages', start, start + count - 1)
			.llen('chat:messages')
			.exec();

		if (messagesErr || lengthErr) {
			throw new Error('Error occurred while messages request');
		}

		return {
			messages: (messages as Array<string>).map(message => JSON.parse(message)) as Array<IMessage>,
			total: length as number,
		};
	}

	public static muteUser(id: string, timeout: number) {
		return redisClient.set(`chat:muted-users:${id}`, 1, 'EX', timeout);
	}

	public static async getUserMuteTimeout(id: string) {
		const value = await redisClient.ttl(`chat:muted-users:${id}`);

		return value > 0 ? value : null;
	}

	public static addTypingUser(id: string) {
		return redisClient.sadd('chat:typing-users', id);
	}

	public static removeTypingUser(id: string) {
		return redisClient.srem('chat:typing-users', id);
	}

	public static async getTypingUsers(): Promise<Array<string>> {
		return new Promise((resolve, reject) => {
			const members = new Set<string>();
			const stream = redisClient.sscanStream('chat:typing-users');
			stream.on('data', (resultMembers: Array<string>) => {
				resultMembers.forEach(member => members.add(member));
			});
			stream.on('end', () => {
				resolve(Array.from(members));
			});
			stream.on('error', err => {
				reject(err);
			});
		});
	}
}
