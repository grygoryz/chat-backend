import { Server, Socket } from 'socket.io';
import { session, authenticate } from '@helpers/middlewares';
import { convertMiddlewareToSocket as convertMiddleware } from '@helpers';
import { registerMainHandlers, registerMessagesHandlers, registerUsersHandlers } from '@socket/handlers';
import { populateUserData, rateLimiter } from '@helpers/middlewares/socket';
import { customEvents } from './mappings';

export const initSocket = (io: Server) => {
	io.use(convertMiddleware(session));
	io.use(convertMiddleware(authenticate));
	io.use(populateUserData);

	io.on('connection', async (socket: Socket) => {
		socket.use(rateLimiter(Object.values(customEvents), socket.handshake.address));

		await registerMainHandlers(io, socket);

		registerUsersHandlers(io, socket);
		registerMessagesHandlers(io, socket);
	});
};
