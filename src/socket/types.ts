import { IUser } from '@odm';
import { StorageFile } from '@types';

export interface IChatUser extends Pick<IUser, 'name' | 'role' | 'about'> {
	id: string;
}

export interface IMessage {
	id: string;
	userId: IChatUser['id'];
	name: IChatUser['name'];
	text: string;
	file?: StorageFile;
	date: number;
}

export type AcknowledgementCb<T> = (err: string | null, value?: T) => void;
