import { ExtendedError } from 'socket.io/dist/namespace';
import { NextFunction, Request, Response } from 'express';
import { Session } from 'express-session';
import { IUser } from '@odm';
import { IChatUser } from '@socket/types';

export interface IUserCredentials {
	email: IUser['email'];
	password: IUser['password'];
}

export interface ICookiePayload {
	id: string;
	role: IUser['role'];
}

export interface StorageFile {
	name: string;
	src: string;
}

export type NextHandlerSocket = (err?: ExtendedError) => void;

export type ExpressMiddleware = (req: Request, res: Response, next: NextFunction) => void;

declare module 'http' {
	interface IncomingMessage {
		session: Session;
	}
}

declare module 'express-session' {
	interface Session {
		user: ICookiePayload;
	}
}

export type SocketMiddleware = (event: [eventName: string, ...args: Array<any>], next: (err?: Error) => void) => void;

declare module 'socket.io' {
	interface Socket {
		userData: IChatUser;
		use(fn: SocketMiddleware): this;
	}
}
