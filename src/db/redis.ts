import debug from 'debug';
import Redis from 'ioredis';

const dg = debug('redis');

const { REDIS_HOSTNAME = '127.0.0.1', REDIS_PORT = '6379' } = process.env;

export const client = new Redis({
	host: REDIS_HOSTNAME,
	port: parseInt(REDIS_PORT, 10),
});

client.on('ready', () => {
	dg(`Connection established on ${REDIS_HOSTNAME}:${REDIS_PORT}`);
});

client.on('end', () => {
	dg('Connection has closed');
});

client.on('error', (err: Error) => {
	dg(`Error occurred with message: ${err.message}`);
});
