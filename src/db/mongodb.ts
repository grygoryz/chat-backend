import mongoose from 'mongoose';
import debug from 'debug';

const dg = debug('db');
const { MONGO_USERNAME, MONGO_PASSWORD, MONGO_HOSTNAME, MONGO_PORT, MONGO_DB, DB_NAME, NODE_ENV } = process.env;

if (NODE_ENV !== 'production') {
	mongoose.set('debug', true);
}

const mongooseOptions = {
	promiseLibrary: global.Promise,
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true,
	// autoIndex: false,
	connectTimeoutMS: 5000,
	socketTimeoutMS: 45000,
	poolSize: 50,
};

const connection = mongoose.connect(
	`mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`,
	mongooseOptions
);

mongoose.connection.on('error', (err: Error) => {
	dg(`Error occurred with message: ${err.message}`);
});

mongoose.connection.on('disconnected', () => {
	dg('Connection has closed');
});

connection
	.then(() => {
		dg(`DB ${DB_NAME} connected`);
	})
	.catch(({ message }) => {
		dg(`DB ${DB_NAME} connected error ${message}`);
	});
