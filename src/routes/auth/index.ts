import { Router } from 'express';
import { authenticate, authorize, bodyValidator } from '@helpers/middlewares';
import { code, user, userCredentials } from '@schemas';
import { roles } from '@odm';
import { register, sendCode, signIn, signOut, verifyUser, check } from './controllers';

const router = Router();

router.post('/register', [bodyValidator(user)], register);
router.post('/signin', [bodyValidator(userCredentials)], signIn);
router.post('/signout', [authenticate], signOut);
router.get('/check', [authenticate], check);

router.post('/verification/codes', [authenticate, authorize([roles.unverified])], sendCode);
router.delete('/verification/codes', [authenticate, authorize([roles.unverified]), bodyValidator(code)], verifyUser);

export { router as auth };
