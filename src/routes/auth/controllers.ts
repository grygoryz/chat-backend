import { Request, Response } from 'express';
import { promisify } from 'util';
import { AuthService } from '@services';
import { roles } from '@odm';

export const register = async (req: Request, res: Response) => {
	try {
		await AuthService.register(req.body);
		res.sendStatus(200);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};

export const signIn = async (req: Request, res: Response) => {
	try {
		const { id, role, name, about } = await AuthService.signIn(req.body);
		req.session.user = { id, role };
		res.status(200).json({ name, about, role, id });
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};

export const signOut = async (req: Request, res: Response) => {
	try {
		await promisify(req.session.destroy).call(req.session);
		res.sendStatus(204);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};

export const check = async (req: Request, res: Response) => {
	try {
		const { id } = req.session.user;
		const { role, name, about } = await AuthService.check(id);
		res.status(200).json({ name, about, role, id });
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};

export const sendCode = async (req: Request, res: Response) => {
	try {
		const { id } = req.session.user;
		await AuthService.sendCode(id);
		res.sendStatus(200);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};

export const verifyUser = async (req: Request, res: Response) => {
	try {
		const { id } = req.session.user;
		const { value } = req.body;
		await AuthService.verifyUser(id, value);
		req.session.user.role = roles.user;
		res.sendStatus(200);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
