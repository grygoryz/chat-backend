import { Request, Router } from 'express';
import { authenticate, authorize } from '@helpers/middlewares';
import { roles } from '@odm';
import multer from 'multer';
import path from 'path';
import { AppError } from '@helpers/errors';
import * as fs from 'fs';
import { uploadFile } from './controllers';

const router = Router();

const multerStorage = multer.diskStorage({
	destination: (req, file, cb) => {
		const storagePath = 'public/storage';
		fs.access(storagePath, err => {
			if (!err) {
				return cb(null, storagePath);
			}

			fs.mkdir(storagePath, { recursive: true }, err => {
				if (!err) {
					return cb(null, storagePath);
				}

				cb(err, '');
			});
		});
	},
	filename: (req, file, cb) => {
		const { name, ext } = path.parse(file.originalname);
		cb(null, `${name}-${Date.now()}${ext}`);
	},
});

const allowedMimeTypes = ['image/jpeg', 'image/png', 'application/pdf', 'image/svg+xml'];

const fileFilter = (req: Request, file: Express.Multer.File, cb: multer.FileFilterCallback) => {
	if (allowedMimeTypes.includes(file.mimetype)) {
		cb(null, true);
	} else {
		cb(new AppError(`Mime type ${file.mimetype} is not allowed`));
	}
};

const upload = multer({
	storage: multerStorage,
	limits: {
		fileSize: 4 * 1024 * 1024, // 4mb
		files: 1,
		fields: 0,
	},
	fileFilter,
});

router.post('/', [authenticate, authorize([roles.user, roles.admin]), upload.single('file')], uploadFile);

export { router as storage };
