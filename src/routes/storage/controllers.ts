import { Request, Response } from 'express';

export const uploadFile = (req: Request, res: Response) => {
	if (req.file) {
		res.status(201).json({
			name: req.file.filename,
			src: req.file.path.replace(/^public/, ''),
		});
	} else {
		res.status(400).json({ message: 'Error while file uploading' });
	}
};
