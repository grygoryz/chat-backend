import { Users } from '@models';

export class UsersService {
	public static async getUserPublicData(id: string) {
		const data = await Users.getPublicDataById(id);

		return data;
	}

	public static async updateBio(id: string, bio: string) {
		await Users.updateBio(id, bio);
	}
}
