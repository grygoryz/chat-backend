import { IUserCredentials } from '@types';
import { sendVerificationEmail } from 'src/mailer';
import { ICode, IUser } from '@odm';
import { Codes, Users } from '@models';

export class AuthService {
	public static async register(data: IUser) {
		await Users.create(data);
	}

	public static async signIn(data: IUserCredentials) {
		return Users.signIn(data);
	}

	public static async check(id: string) {
		const data = await Users.getPublicDataById(id);

		return data;
	}

	public static async sendCode(id: string) {
		const verified = await Users.getVerificationStatusById(id);

		if (verified) {
			throw new Error('User already verified');
		}

		const code = await Codes.create(id);
		const email = await Users.getEmailById(id);
		await sendVerificationEmail(email, code);
	}

	public static async verifyUser(id: string, code: ICode['value']) {
		const verified = await Users.getVerificationStatusById(id);

		if (verified) {
			throw new Error('User already verified');
		}

		await Codes.verify(id, code);
		await Users.verify(id);
	}
}
