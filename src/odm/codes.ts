import mongoose, { Document } from 'mongoose';

export const mappings = {
	retryTimeout: 60 * 1000, // 1m
};

export interface ICode {
	value: string;
	user: string;
	createdAt: string;
}

export interface ICodeModel extends ICode, Document {}

const schema = new mongoose.Schema<ICodeModel>({
	value: {
		type: String,
		index: 1,
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'users',
	},
	createdAt: {
		type: Date,
		default: Date.now,
	},
});

schema.index({ createdAt: 1 }, { expireAfterSeconds: 600 });

export const codes = mongoose.model<ICodeModel>('codes', schema);
