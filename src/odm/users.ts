import mongoose, { Document } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

export const mappings = {
	genders: ['male', 'female', 'other'] as const,
	nameLength: { min: 5, max: 30 },
	descriptionLength: { min: 35, max: 255 },
};

export const roles = {
	unverified: 'N' as const,
	user: 'U' as const,
	admin: 'A' as const,
};

export type Role = typeof roles[keyof typeof roles];

type Gender = typeof mappings.genders[number];

export interface IUser {
	name: string;
	email: string;
	password: string;
	role: Role;
	about: {
		gender: Gender;
		bio?: string;
	};
}

export interface IUserModel extends IUser, Document {}

const schema = new mongoose.Schema<IUserModel>(
	{
		name: {
			type: String,
			required: true,
			unique: true,
			trim: true,
			minlength: [mappings.nameLength.min, 'Username is too short'],
			maxLength: [mappings.nameLength.max, 'Username is too long'],
		},
		email: {
			type: String,
			required: true,
			unique: true,
			match: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
		},
		password: {
			type: String,
			required: true,
		},
		role: {
			type: String,
			enum: Object.values(roles),
			default: roles.unverified,
		},
		about: {
			gender: {
				type: String,
				enum: mappings.genders,
				required: true,
			},
			bio: {
				type: String,
				minlength: [mappings.descriptionLength.min, 'Description is too short'],
				maxlength: [mappings.descriptionLength.max, 'Description is too long'],
			},
		},
	},
	{ timestamps: true }
);

schema.plugin(uniqueValidator, { message: 'already exists' });

export const users = mongoose.model<IUserModel>('users', schema);
