import { Socket } from 'socket.io';
import { Request, Response, NextFunction } from 'express';
import { ExpressMiddleware, NextHandlerSocket } from '@types';

export const convertMiddlewareToSocket = (fn: ExpressMiddleware) => (socket: Socket, next: NextHandlerSocket) =>
	fn(socket.request as Request, {} as Response, next as NextFunction);
