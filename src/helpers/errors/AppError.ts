export class AppError extends Error {
	readonly statusCode: number;

	constructor(message: string, statusCode = 500) {
		super(message);
		Object.setPrototypeOf(this, new.target.prototype);

		if (!(statusCode >= 100 && statusCode <= 599)) {
			throw new Error('statusCode must be a number in range from 100 to 599');
		}

		Error.captureStackTrace(this, AppError);
		this.statusCode = statusCode;
	}
}
