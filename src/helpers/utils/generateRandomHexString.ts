import { promisify } from 'util';
import * as crypto from 'crypto';

export const generateRandomHexString = async (size: number) => {
	const buf = await promisify(crypto.randomBytes).call(crypto, size);

	return buf.toString('hex');
};
