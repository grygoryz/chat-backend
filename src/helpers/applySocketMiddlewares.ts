import { Socket } from 'socket.io';
import { SocketMiddleware } from '@types';

export const applySocketMiddlewares = (socket: Socket, middlewares: Array<SocketMiddleware>) => {
	middlewares.forEach(middleware => socket.use(middleware));
};
