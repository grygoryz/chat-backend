import { Role } from '@odm';
import { ExpressMiddleware } from '@types';
import { AppError } from '../errors';

export const authorize = (allowedRoles: Array<Role>): ExpressMiddleware => (req, res, next) => {
	const { role } = req.session.user;

	if (allowedRoles.includes(role)) {
		next();
	} else {
		next(new AppError('Not authorized', 403));
	}
};
