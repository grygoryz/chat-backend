import { ExpressMiddleware } from '@types';

export const cacheControl: ExpressMiddleware = (req, res, next) => {
	res.set('Cache-Control', 'no-cache, no-store, must-revalidate');
	next();
};
