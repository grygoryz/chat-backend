import { Socket } from 'socket.io';
import { UsersService } from '@services';

export const populateUserData = async (socket: Socket, next: (err?: Error | undefined) => void) => {
	try {
		const userId = socket.request.session.user.id;
		const userData = await UsersService.getUserPublicData(userId);
		socket.userData = { id: userId, ...userData };
		next();
	} catch ({ message }) {
		next(new Error(message));
	}
};
