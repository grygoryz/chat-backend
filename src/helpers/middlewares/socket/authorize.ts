import { Role } from '@odm';
import { SocketMiddleware } from '@types';

export const authorize = (options: Array<{ events: Array<string>; allowedRoles: Array<Role> }>, role: Role): SocketMiddleware => (
	event,
	next
) => {
	const matchedOption = options.find(({ events }) => events.includes(event[0]));
	if (matchedOption && !matchedOption.allowedRoles.includes(role)) {
		return next(new Error(`You have not enough rights for executing event ${event[0]}`));
	}
	next();
};
