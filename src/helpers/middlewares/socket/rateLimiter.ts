import { SocketMiddleware } from '@types';
import { RateLimiterRedis } from 'rate-limiter-flexible';
import { client as redisClient } from '@db/redis';

const rateLimiterRedis = new RateLimiterRedis({
	storeClient: redisClient,
	keyPrefix: 'socket_limiter',
	points: 10,
	duration: 1,
});

export const rateLimiter = (events: Array<string>, ip: string): SocketMiddleware => async (event, next) => {
	if (events.includes(event[0])) {
		try {
			await rateLimiterRedis.consume(ip);
			return next();
		} catch {
			return next(new Error('Too many requests'));
		}
	}
	next();
};
