import { SocketMiddleware } from '@types';

export const acknowledgementRequired = (events: Array<string>): SocketMiddleware => (event, next) => {
	if (events.includes(event[0]) && typeof event[event.length - 1] !== 'function') {
		return next(new Error(`Acknowledgement required for event ${event[0]}`));
	}
	next();
};
