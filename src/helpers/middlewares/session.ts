import { sessionOptions } from '@config';
import expressSession from 'express-session';

export const session = expressSession(sessionOptions);
