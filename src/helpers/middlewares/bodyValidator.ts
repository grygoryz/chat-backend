import { ExpressMiddleware } from '@types';
import { validate } from '@helpers';
import { AppError } from '../errors';

export const bodyValidator = (schema: object): ExpressMiddleware => (req, res, next) => {
	const errors = validate(schema, req.body);

	if (!errors) {
		next();
	} else {
		next(new AppError(`request body is not valid. Errors: ${errors}`, 400));
	}
};
