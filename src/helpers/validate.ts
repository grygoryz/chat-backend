import Ajv from 'ajv';

export const validate = (schema: object, value: any) => {
	const ajv = new Ajv({ allErrors: true });
	const validator = ajv.compile(schema);
	const valid = validator(value);

	if (!valid) {
		return validator
			.errors!.map(({ message, dataPath }) => {
				return `${dataPath ? `(${dataPath.slice(1)}) ` : ''}${message}`;
			})
			.join(', ');
	}
};
