import bcrypt from 'bcrypt';
import { IUser, IUserModel, users, roles } from '@odm';
import { IUserCredentials } from '@types';

export class Users {
	public static async create(data: IUser) {
		const { password, name, about, email } = data;
		const passwordHash = await bcrypt.hash(password, 11);
		await users.create({ password: passwordHash, name, about, email });
	}

	public static async signIn(data: IUserCredentials) {
		const { email, password } = data;
		const user: IUserModel = await users.findOne({ email }).lean();

		if (!user) {
			throw new Error('Credentials are not valid');
		}

		const valid = await bcrypt.compare(password, user.password);

		if (!valid) {
			throw new Error('Credentials are not valid');
		}

		const { _id: id, role, name, about } = user;

		return { id, role, name, about };
	}

	public static async getPublicDataById(id: string) {
		const data = await Users.getDataById(id, ['name', 'role', 'about']);

		return data;
	}

	public static async getEmailById(id: string) {
		const { email } = await Users.getDataById(id, ['email']);

		return email;
	}

	public static async getVerificationStatusById(id: string) {
		const { role } = await Users.getDataById(id, ['role']);

		return role !== roles.unverified;
	}

	public static async verify(id: string) {
		await users.findByIdAndUpdate(id, { role: roles.user });
	}

	private static async getDataById<T extends keyof IUser>(id: string, fields: Array<T>): Promise<Pick<IUser, T>> {
		const projection = `${fields.join(' ')} -_id`;
		const user = await users.findById(id).select(projection).lean();

		if (!user) {
			throw new Error('User not found');
		}

		return user;
	}

	public static async updateBio(id: string, bio: string) {
		await users.updateOne({ _id: id }, { 'about.bio': bio }, { runValidators: true });
	}
}
