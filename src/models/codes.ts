import { codes, codesMappings, ICode } from '@odm';
import { generateRandomHexString } from '@helpers/utils';

export class Codes {
	public static async create(user: ICode['user']): Promise<ICode['value']> {
		const data: Pick<ICode, 'createdAt'> = await codes.findOne({ user }).select('-_id createdAt').lean();
		if (data && new Date(data.createdAt).getTime() + codesMappings.retryTimeout > Date.now()) {
			throw new Error('Retry timeout has not expired');
		}

		const value = await generateRandomHexString(3);
		await codes.updateOne({ user }, { value, createdAt: Date.now().toString() }, { upsert: true });

		return value;
	}

	public static async verify(id: ICode['user'], code: ICode['value']) {
		const res = await codes.findOne({ value: code }).select('user');

		if (res?.user.toString() !== id) {
			throw new Error("Code is wrong or doesn't exist");
		}

		await res.remove();
	}
}
