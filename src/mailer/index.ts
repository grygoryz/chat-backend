import nodemailer from 'nodemailer';

const { MAILER_SERVICE, MAILER_USER, MAILER_PASS, MAILER_FROM } = process.env;

const transporter = nodemailer.createTransport({
	service: MAILER_SERVICE,
	auth: {
		user: MAILER_USER,
		pass: MAILER_PASS,
	},
});

export const sendVerificationEmail = async (email: string, code: string): Promise<void> => {
	const options = {
		from: MAILER_FROM,
		to: email,
		subject: 'Verification code',
		text: `Your verification code: ${code}`,
	};

	await transporter.sendMail(options);
};
