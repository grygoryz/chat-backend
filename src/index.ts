import debug from 'debug';
import { createServer } from 'http';
import { Server as SocketIOServer } from 'socket.io';
import mongoose from 'mongoose';
import { createTerminus } from '@godaddy/terminus';
import { client as redisClient } from '@db/redis';
import { socketIOConfig } from '@config';
import { initSocket } from '@socket';
import { app } from './server';

import './db/mongodb';

const dg = debug('server');
const { PORT } = process.env;

const httpServer = createServer(app);

const io = new SocketIOServer(httpServer, socketIOConfig);
initSocket(io);

httpServer.listen(PORT, () => {
	dg(`Server listens on port ${PORT}`);
});

createTerminus(httpServer, {
	signals: ['SIGINT', 'SIGTERM'],
	onSignal: () => {
		dg('Cleanup started');
		return Promise.all([mongoose.disconnect(), redisClient.quit()]);
	},
	onShutdown: async () => {
		dg('Cleanup finished, server is down');
	},
});
